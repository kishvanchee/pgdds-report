# -*- coding: utf-8 -*-
"""
Created on Wed Jun  6 00:28:40 2018

@author: Kishore
"""

# %%
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# %%
clean_crimes = pd.read_pickle('../src/clean_crimes')
clean_crimes['Community Area'] = clean_crimes['Community Area'].fillna(value = 0).astype('uint8')

# %%
top_crimes = clean_crimes.groupby(['Location Description']).size().sort_values(ascending=False).rename('count').head(15)

# %%
fig, ax = plt.subplots()
top_crimes.plot(kind='barh', color = 'steelblue', rot=0, title = 'Top places of crimes')
plt.xlabel('Number of Crimes')
plt.ylabel('Place of Crime')
plt.tight_layout()
plt.tick_params(axis='both', which='major', labelsize=7)
plt.tick_params(axis='both', which='minor', labelsize=7)
fig.savefig('../images/general/topPlacesCount.png')

# %%
#top_places_df = clean_crimes.copy()

places = top_crimes.index.astype('str').tolist()
top_places_df = clean_crimes[clean_crimes['Location Description'].isin(places)]

# %%
date_wise_places_cnt = pd.crosstab(top_places_df['date'], top_places_df['Location Description'].astype('str')).rename_axis(None, axis = 1)


# %%
date_wise_places_cnt.columns = date_wise_places_cnt.columns.str.replace("/", " or ")
places = date_wise_places_cnt.columns
for place in places:
    fig, ax = plt.subplots()
    ts_plot = date_wise_places_cnt[[place]].plot(ax=ax, title = place, legend=False)
    plt.xlabel('Day')
    plt.ylabel('Count of crimes')
    fig.savefig('../images/placeCount/' + place.title().replace(' ', '-').replace('"', '') + '.png')


# %%
places = date_wise_places_cnt.columns
for place in places:
    fig, ax = plt.subplots()
    ts_plot = date_wise_places_cnt[[place]].rolling(window=365).mean().plot(ax=ax, title = place + ' - Rolling mean', legend=False)
    plt.xlabel('Day')
    plt.ylabel('Avg crimes')
    fig.savefig('../images/rollingMeanPlace/' + place.title().replace(' ', '-').replace('\"', '') + '.png')

# %%
places = date_wise_places_cnt.columns
for place in places:
    fig, ax = plt.subplots()
    diff_plot = date_wise_places_cnt[place].diff().plot(ax=ax, title = place)
    plt.xlabel('Date')
    plt.ylabel('')
    plt.suptitle('First order difference')
    fig.savefig('../images/fodPlaces/' + place.title().replace(' ', '-').replace('\"', '') + '.png')
