# -*- coding: utf-8 -*-
"""
Created on Wed Jun  6 16:12:23 2018

@author: Administrator
"""

# %%
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# %%
clean_crimes = pd.read_pickle('../src/clean_crimes')
clean_crimes['Community Area'] = clean_crimes['Community Area'].fillna(value = 0).astype('uint8')

# %%
arrests_yes = clean_crimes[clean_crimes['Arrest'] == True]
arrests_no = clean_crimes[clean_crimes['Arrest'] == False]

# %%
arrests_yes.set_index('date', inplace = True)
arrests_no.set_index('date', inplace = True)

# %%
arrests_no_cnt = arrests_no.groupby(['date'])['day_of_week'].agg(['count']).reset_index()
arrests_no_cnt.columns = ['date', 'no']
arrests_yes_cnt = arrests_yes.groupby(['date'])['day_of_week'].agg(['count']).reset_index()
arrests_yes_cnt.columns = ['date', 'yes']

# %%
arrests = pd.merge(arrests_no_cnt, arrests_yes_cnt, on='date')
arrests.set_index('date', inplace = True)

# %%
fig, ax = plt.subplots()
arrests.plot(ax = ax, title = 'Number of arrests per day')
plt.xlabel('Date')
plt.ylabel('Count')
plt.savefig('../images/arrestsGeneral/dailyArrests.png')

# %%
fig, ax = plt.subplots()
arrests.plot.area(ax=ax, title = 'Number of arrests per day')
plt.xlabel('Date')
plt.ylabel('Arrests')
plt.savefig('../images/arrestsGeneral/dailyArrestsArea.png')

# %%
fig, ax = plt.subplots()
arrests.rolling(365).mean().plot(ax = ax, title = 'Rolling mean of whether an arrest was made')
plt.xlabel('Date')
plt.ylabel('Rolling avg')
plt.savefig('../images/arrestsGeneral/rollingMean.png')

# %% arrests for each crime
top_crimes = clean_crimes.groupby(['Primary Type']).size().sort_values(ascending=False).rename('count').head(15)
crimes = top_crimes.index.astype('str').tolist()
top_crimes_df = clean_crimes[clean_crimes['Primary Type'].isin(crimes)]

# %%
top_crimes_arrests = pd.crosstab(top_crimes_df['Primary Type'].astype('str'), top_crimes_df['Arrest']).rename_axis(None, axis = 1)
top_crimes_arrests.columns = ['no', 'yes']

# %%
fig, ax = plt.subplots()
top_crimes_arrests.plot(rot = 0, kind = 'barh', ax = ax, title = 'Number of arrests for top 15 crimes')
plt.xlabel('Count')
plt.ylabel('Type of crime')
plt.tight_layout()
plt.savefig('../images/arrestsGeneral/arrestsPerCrime.png')

# %%
for crime in top_crimes.index.tolist():
    subset = top_crimes_df[top_crimes_df['Primary Type'] == crime]
    
    subset = pd.crosstab(subset['date'], subset['Arrest'].astype('str')).rename_axis(None, axis = 1)
        
    subset.columns = ['no', 'yes']
    fig, ax = plt.subplots()
    subset.rolling(365).mean().plot(ax = ax, title = 'Rolling avg of arrests for ' + crime)
    plt.xlabel('Date')
    plt.ylabel('Rolling avg')
    plt.savefig('../images/arrestsCrimes/' + crime.title().replace(' ', '-') + '.png')

# %% arrests for each place of crime
top_places = clean_crimes.groupby(['Location Description']).size().sort_values(ascending=False).rename('count').head(15)
places = top_places.index.astype('str').tolist()
top_places_df = clean_crimes[clean_crimes['Location Description'].isin(places)]

# %%
top_places_arrests = pd.crosstab(top_places_df['Location Description'].astype('str'), top_places_df['Arrest']).rename_axis(None, axis = 1)
top_places_arrests.columns = ['no', 'yes']

# %%
fig, ax = plt.subplots()
top_places_arrests.plot(ax=ax, rot = 0, kind = 'barh', title = 'Arrests for top 15 places of crimes')
plt.xlabel('Count')
plt.ylabel('Place of crime')
plt.tight_layout()
plt.savefig('../images/arrestsGeneral/arrestsPerPlace.png')

# %%
for place in top_places.index.tolist():
    subset = top_places_df[top_places_df['Location Description'] == place]
    
    subset = pd.crosstab(subset['date'], subset['Arrest'].astype('str')).rename_axis(None, axis = 1)
        
    subset.columns = ['no', 'yes']
    
    fig, ax = plt.subplots()
    subset.rolling(365).mean().plot(ax=ax, title = 'Rolling avg of arrests at ' + place)
    plt.xlabel('Date')
    plt.ylabel('Rolling avg')
    plt.savefig('../images/arrestsPlaces/' + place.title().replace(' ', '-').replace('/', '-') + '.png')

