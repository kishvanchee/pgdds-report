# -*- coding: utf-8 -*-
"""
Created on Wed Jun  6 00:28:40 2018

@author: Kishore
"""

# %%
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# %%
clean_crimes = pd.read_pickle('../src/clean_crimes')
clean_crimes['Community Area'] = clean_crimes['Community Area'].fillna(value = 0).astype('uint8')

# %%
top_crimes = clean_crimes.groupby(['Primary Type']).size().sort_values(ascending=False).rename('count').head(15)

# %%
fig, ax = plt.subplots()
top_crimes.plot(kind='barh', color = 'steelblue', rot=0, title = 'Top crimes')
plt.xlabel('Number of Crimes')
plt.ylabel('Type of Crime')
plt.tight_layout()
plt.tick_params(axis='both', which='major', labelsize=7)
plt.tick_params(axis='both', which='minor', labelsize=7)
fig.savefig('../images/general/topCrimesCount.png')

# %%
#top_crimes_df = clean_crimes.copy()

crimes = top_crimes.index.astype('str').tolist()
top_crimes_df = clean_crimes[clean_crimes['Primary Type'].isin(crimes)]


# %%
date_wise_crimes_cnt = pd.crosstab(top_crimes_df['date'], top_crimes_df['Primary Type'].astype('str')).rename_axis(None, axis = 1)


# %%
crimes = date_wise_crimes_cnt.columns
for crime in crimes:
    fig, ax = plt.subplots()
    ts_plot = date_wise_crimes_cnt[[crime]].plot(ax=ax, title = crime, legend=False)
    plt.xlabel('Day')
    plt.ylabel('Count of crimes')
    fig.savefig('../images/crimesCount/' + crime.title().replace(' ', '-') + '.png')


# %%
crimes = date_wise_crimes_cnt.columns
for crime in crimes:
    fig, ax = plt.subplots()
    ts_plot = date_wise_crimes_cnt[[crime]].rolling(window=365).mean().plot(ax=ax, title = crime + ' - Rolling mean', legend=False)
    plt.xlabel('Day')
    plt.ylabel('Avg crimes')
    fig.savefig('../images/rollingMean/' + crime.title().replace(' ', '-') + '.png')

# %%
crimes = date_wise_crimes_cnt.columns
for crime in crimes:
    fig, ax = plt.subplots()
    diff_plot = date_wise_crimes_cnt[crime].diff().plot(ax=ax, title = crime)
    plt.xlabel('Date')
    plt.ylabel('')
    plt.suptitle('First order difference')
    fig.savefig('../images/fodCrimes/' + crime.title().replace(' ', '-') + '.png')
