# -*- coding: utf-8 -*-
"""
Created on Wed Mar 21 11:46:44 2018

@author: Kishore
"""
#%%
import numpy as np
import pandas as pd

#%%
def mem_usage(pandas_obj):
    if isinstance(pandas_obj,pd.DataFrame):
        usage_b = pandas_obj.memory_usage(deep=True).sum()
    else: # we assume if not a df it's a series
        usage_b = pandas_obj.memory_usage(deep=True)
    usage_mb = usage_b / 1024 ** 2 # convert bytes to megabytes
    return "{:03.2f} MB".format(usage_mb)

#%%
col_to_use = ['Date', 'Block',
              #'IUCR',
              'Primary Type',
              'Location Description',
              'Arrest',
              'Domestic',
              'Beat',
              'District',
              'Ward',
              'Community Area',
              'X Coordinate',
              'Y Coordinate',
              'Latitude',
              'Longitude']

#%%
data_types = {'Block' : 'category',
              #'IUCR' : 'category',
              'Primary Type' : 'category',
              'Location Description': 'category',
              'Arrest':'bool',
              'Domestic': 'bool',
              'Beat' : 'uint16',
              'District': 'float32',
              'Ward' : 'float32',
              'Community Area' : 'float32',
              'X Coordinate' : 'float32',
              'Y Coordinate' : 'float32',
              'Latitude': 'float32',
              'Longitude' : 'float32'
             }

#%%
raw_crimes = pd.read_csv("../src/Crimes_2001_to_present.csv",
                         usecols=col_to_use,
                         dtype=data_types)
#%%
clean_crimes = raw_crimes.copy()

#%%
clean_crimes['dte'] = pd.to_datetime(clean_crimes['Date'],
            format = '%m/%d/%Y %I:%M:%S %p')

clean_crimes.sort_values(by = 'dte', inplace = True)

clean_crimes = clean_crimes.reset_index(drop=True)

#%%
clean_crimes['date'] = clean_crimes['dte'].dt.date.astype('datetime64[ns]')
clean_crimes['year'] = clean_crimes['dte'].dt.year.astype('uint16')
clean_crimes['quarter'] = clean_crimes['dte'].dt.quarter.astype('uint8')
clean_crimes['month'] = clean_crimes['dte'].dt.month.astype('uint8')
clean_crimes['week_of_year'] = clean_crimes['dte'].dt.week.astype('uint8')
clean_crimes['day_of_year'] = clean_crimes['dte'].dt.dayofyear.astype('uint16')
clean_crimes['day_of_month'] = clean_crimes['dte'].dt.day.astype('uint8')
clean_crimes['day_of_week'] = clean_crimes['dte'].dt.dayofweek.astype('uint8')
clean_crimes['hour'] = clean_crimes['dte'].dt.hour.astype('uint8')

clean_crimes.drop(['Date', 'dte'], axis = 1, inplace = True)
#%%
clean_crimes.to_csv("../src/clean_crimes.csv", index = False)

#print(clean_crimes.head())

#print(mem_usage(clean_crimes))
