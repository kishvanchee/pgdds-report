# -*- coding: utf-8 -*-
"""
Created on Wed Mar 21 13:16:21 2018

@author: Kishore
"""

#%%
import numpy as np
import pandas as pd

#%%
data_types = {'Block' : 'category',
              'Primary Type' : 'category',
              'Location Description': 'category',
              'Arrest':'bool',
              'Domestic': 'bool',
              'Beat' : 'uint16',
              'District': 'float32',
              'Ward' : 'float32',
              'Community Area' : 'float32',
              'X Coordinate' : 'float32',
              'Y Coordinate' : 'float32',
              'Latitude': 'float32',
              'Longitude' : 'float32',
              'year' : 'uint16',
              'quarter' : 'uint8',
              'month' : 'uint8',
              'week_of_year' : 'uint8',
              'day_of_year' : 'uint16',
              'day_of_month' : 'uint8',
              'day_of_week' : 'uint8',
              'hour' : 'uint8'
             }

#%%

clean_crimes = pd.read_csv("../src/clean_crimes.csv",
                           dtype=data_types,
                           parse_dates = True)

#%%
clean_crimes['date'] = pd.to_datetime(clean_crimes['date'], format = '%Y-%m-%d')
