# -*- coding: utf-8 -*-
"""
Created on Wed Jun  6 01:03:07 2018

@author: Kishore
"""

# %%
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

# %%
clean_crimes = pd.read_pickle('../src/clean_crimes')
clean_crimes['Community Area'] = clean_crimes['Community Area'].fillna(value = 0).astype('uint8')

# %%
clean_crimes.set_index('date', inplace=True)

# %%
day_wise_count = clean_crimes.groupby(['date'])['day_of_week'].agg(['count']).reset_index()

# %%
fig, ax = plt.subplots()
plt.plot(day_wise_count['date'], day_wise_count['count'])
plt.title('Daily crimes')
plt.xlabel('Date')
plt.ylabel('Count of crimes')
fig.savefig('../images/general/dailyCrimes.png')

# %%
fig, ax = plt.subplots()
clean_crimes.resample('D').size().rolling(365).mean().plot(ax = ax, title = 'All Crimes - Rolling mean')
plt.xlabel('Day')
plt.ylabel('Rolling avg crimes')
fig.savefig('../images/general/rollingMeanCrimes.png')

# %%
day_wise_count.set_index('date', inplace=True)

# %%
fig, ax = plt.subplots()
day_wise_count.diff().plot(ax = ax, title = 'First order differencing for all crimes', legend = False)
plt.xlabel('Date')
plt.ylabel('')
fig.savefig('../images/general/fodAllCrimes.png')