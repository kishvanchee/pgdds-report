%\setcounter{page}{1}
%\pagenumbering{arabic}

\chapter{Exploratory Data Analysis}

%\blindtext

Exploratory data analysis (EDA) is a very important step which takes place after feature engineering and acquiring data and it should be done before any modeling. This is because it is very important for a data scientist to be able to understand the nature of the data without making assumptions.

The purpose of EDA is to use summary statistics and visualizations to better understand data, and find clues about the tendencies of the data, its quality and to formulate assumptions and the hypothesis of our analysis. EDA is NOT about making fancy visualizations or even aesthetically pleasing ones, the goal is to try and answer questions with data. Your goal should be to be able to create a figure which someone can look at in a couple of seconds and understand what is going on. If not, the visualization is too complicated (or fancy) and something similar should be used.

EDA is also very iterative since we first make assumptions based on our first exploratory visualizations, then build some models. We then make visualizations of the model results and tune our models.

EDA is not identical to statistical graphics although the two terms are used almost interchangeably. Statistical graphics is a collection of techniques--all graphically based and all focusing on one data characterization aspect. EDA encompasses a larger venue; EDA is an approach to data analysis that postpones the usual assumptions about what kind of model the data follow with the more direct approach of allowing the data itself to reveal its underlying structure and model. EDA is not a mere collection of techniques; EDA is a philosophy as to how we dissect a data set; what we look for; how we look; and how we interpret. It is true that EDA heavily uses the collection of techniques that we call "statistical graphics", but it is not identical to statistical graphics per se.

Most EDA techniques are graphical in nature with a few quantitative techniques. The reason for the heavy reliance on graphics is that by its very nature the main role of EDA is to open-mindedly explore, and graphics gives the analysts unparalleled power to do so, enticing the data to reveal its structural secrets, and being always ready to gain some new, often unsuspected, insight into the data. In combination with the natural pattern-recognition capabilities that we all possess, graphics provides, of course, unparalleled power to carry this out.

The particular graphical techniques employed in EDA are often quite simple, consisting of various techniques of:

\begin{enumerate}
	\item Plotting the raw data (such as data traces, histograms, bihistograms, probability plots, lag plots, block plots, and Youden plots.
	\item Plotting simple statistics such as mean plots, standard deviation plots, box plots, and main effects plots of the raw data.
	\item Positioning such plots so as to maximize our natural pattern-recognition abilities, such as using multiple plots per page.
\end{enumerate}

\section{Objectives of EDA}

The objectives of EDA are to:

\begin{itemize}
	\item Suggest hypotheses about the causes of observed phenomena
	\item Assess assumptions on which statistical inference will be based
	\item Support the selection of appropriate statistical tools and techniques
	\item Provide a basis for further data collection through surveys or experiments
\end{itemize}

\section{Guidelines for EDA}

General guidelines to be followed for EDA:

\begin{itemize}
	\item Check number of rows and columns i.e. dimension of the data set.
	\item Understand what each row and column means/stands for.
	\item Calculate the number/percentage of nulls in each column.
	\item Identify the type of each column.
	\item Date
	\begin{itemize}
		\item Year, Month, Day
		\item Hour, Minute, Second
		\item Season, Quarter, Day of week, Week of year
	\end{itemize}
\end{itemize}

\section{Choice of tool}

\subsection{R}

Initially, the data set was loaded using R \& RStudio. The size of the data set is \SI{1.5}{\giga\byte}. The size of RAM in the system used is \SI{8}{\giga\byte}. It was observed that,

\begin{itemize}
	\item Takes too long to load data into memory.
	\begin{itemize}
		\item Base R \code{read.csv} is really slow, benchmark around 30 to 45 minutes to load.
		\item Package \code{readr} \code{read\_csv} is faster to 
		\item Package \code{data.table} \code{fread} is also faster
	\end{itemize}
	\item Performance wise, simple date-time manipulation hangs RStudio.
	\item Long time taken for visualizations; messy visualizations.
\end{itemize}

Problems with large data sets in R:
\begin{itemize}
	\item R reads entire data set into RAM all at once. Other programs can read file sections on demand.
	\item R Objects live in memory entirely.
	\item Does not have int64 datatype
	\item Not possible to index objects with huge numbers of rows \& columns even in 64 bit systems (2 Billion vector index limit) . Hits file size limit around \SIrange{2}{4}{\giga\byte}.
\end{itemize}

Ways to handle large data sets:
\begin{itemize}
	\item Make the data smaller - not possible since it is time series data
	\item Get a bigger computer - not possible due to lack of resources
	\item Access the data differently - possible way is to use RDBMS, or cloud services
	\item Split up the dataset for analysis - not possible since it is time series data
	\item Parallelize - not possible due to lack of clusters
	\item Use different tool like Python, Hadoop, etc - possible to use python
\end{itemize}


\subsection{Python}

The performance of python was observed to be better than R by a large amount. Hence, python has been chosen as the choice of tool for data munging.

In python, each variable read into the pandas API is assigned \code{object} as the data type, as default. This causes the variables and the values to take up unnecessary space since the memory allocated to it will be quite big. Thus, we use the \emph{numpy} library to make sure we assign the proper datatypes for the variables in the data set.

The RAM usage before assigning proper data types was \SI{5.3}{\giga\byte}. The RAM usage after assigning proper data types was \SI{369.45}{\mega\byte}.

\subsubsection{numpy data types}

\begin{table}[H]
	\centering
	\caption{numpy DataTypes\label{numpyDataTypes}}
		\begin{tabular}{ l l r}
			\toprule
			Data type & Description & Size\\
			\midrule
						
			Int8 & Byte & (-128 to 127)\\
			Int16 & Integer & (-32768 to 32767)\\
			Int32 & Integer & (-2147483648 to 2147483647)\\
			bool\_ & Boolean & (True or False) stored as a byte\\
			Int64 & Integer & (-9223372036854775808 to 9223372036854775807)\\
			Uint8 & Unsigned & (0 to 255)\\
			Uint16 & Unsigned & (0 to 65535)\\
			Uint32 & Unsigned & (0 to 4294967295)\\
			Uint64 & Unsigned & (0 to 18446744073709551615)\\
			
			\bottomrule
		\end{tabular}
\end{table}

\section{Data Cleaning and Transformation}

%\blindtext

\subsection{Initial exploration with bash}

\begin{lstlisting}[language = bash, numberstyle=\tiny, xleftmargin=2em,frame=single,framexleftmargin=1.5em, numbers = left, numbersep = 5pt, caption = {Exploration with bash}, captionpos = b]
head -2 Crimes_2001_to_present.csv
tail -2 Crimes_2001_to_present.csv
wc -l Crimes_2001_to_present.csv
\end{lstlisting}

\begin{figure}[H]
\caption{Using bash to get glimpse of data}
\label{fig:bash}
\centering
\includegraphics[scale = 0.55]{head_tail_bash}
\end{figure}

\subsection{Read data with python}

\lstinputlisting[language = Python, breaklines=true, xleftmargin=2em,frame=single,framexleftmargin=1.5em, numberstyle=\tiny, numbers = left, numbersep = 5pt, caption={Reading data with python}, captionpos = b]{scripts/readingdata.py}
%\lstinputlisting{/home/kishore/kishore\_repositories/crimes-chicago/py\_scripts/reading\_data.py}

\subsection{Clean data with python}

\lstinputlisting[language = Python, breaklines=true, xleftmargin=2em,frame=single,framexleftmargin=1.5em, numberstyle=\tiny, numbers = left, numbersep = 5pt, caption = {Clean data with python}]{scripts/cleaning.py}

\subsection{File format}

The Dataframe is saved in pickle format, after cleaning/munging, rather than comma separated values (CSV) for faster read/write speeds. The benchmarks are as follows.

\begin{itemize}
	\item CSV (before optimization) - \SI{1}{\minute} \SI{2}{\second} $\pm$ \SI{29.8}{\second}.
	\item CSV (after optimization) - \SI{34.4}{\second} $\pm$ \SI{5.09}{\second}.
	\item Pickle - \SI{475}{\milli\second} $\pm$ \SI{19}{\milli\second}.
\end{itemize}

\newpage
\section{Data Exploration}

%\blindtext

\lstinputlisting[language = Python, breaklines=true, xleftmargin=2em,frame=single,framexleftmargin=1.5em, numberstyle=\tiny, numbers = left, numbersep = 5pt, caption = {Exploration of data}, label = {lst:generalcrimes}]{scripts/generalcrimests.py}

\begin{center}
	\includegraphics[width=0.9\linewidth]{general/dailyCrimes.png}\\
	\captionof{figure}{Count of Daily Crimes}
	\label{fig:dailyCount}
	
	\includegraphics[width=0.9\linewidth]{general/topCrimesCount.png}\\
	\captionof{figure}{Count of top 15 crimes}
	\label{fig:topCrimesCount}
	
	\includegraphics[width=0.9\linewidth]{general/topPlacesCount.png}\\
	\captionof{figure}{Count of top 15 places crimes were committed}
	\label{fig:topPlacesCount}
\end{center}

\newpage
\section{Daily Count of Crimes}

\lstinputlisting[language = Python, breaklines=true, xleftmargin=2em,frame=single,framexleftmargin=1.5em, numberstyle=\tiny, numbers = left, numbersep = 5pt, caption = {Exploration of different crimes}, label = {lst:topcrimes}]{scripts/topcrimests.py}

\begin{center}
	\input{tex/crimesCount.tex}
\end{center}

\newpage
\section{Daily count of crimes in different places}

\lstinputlisting[language = Python, breaklines=true, xleftmargin=2em,frame=single,framexleftmargin=1.5em, numberstyle=\tiny, numbers = left, numbersep = 5pt, caption = {Exploration of different places of crimes}, label = {lst:topplaces}]{scripts/topplacests.py}

\begin{center}
	\input{tex/placeCount.tex}
\end{center}

\newpage
\section{Rolling Mean}

In statistics, a moving average (rolling average or running average) is a calculation to analyze data points by creating series of averages of different subsets of the full data set. It is also called a moving mean (MM) or rolling mean and is a type of finite impulse response filter. Variations include: simple, and cumulative, or weighted forms (described below).

Given a series of numbers and a fixed subset size, the first element of the moving average is obtained by taking the average of the initial fixed subset of the number series. Then the subset is modified by "shifting forward"; that is, excluding the first number of the series and including the next value in the subset.

A moving average is commonly used with time series data to smooth out short-term fluctuations and highlight longer-term trends or cycles. The threshold between short-term and long-term depends on the application, and the parameters of the moving average will be set accordingly. For example, it is often used in technical analysis of financial data, like stock prices, returns or trading volumes. It is also used in economics to examine gross domestic product, employment or other macroeconomic time series. Mathematically, a moving average is a type of convolution and so it can be viewed as an example of a low-pass filter used in signal processing. When used with non-time series data, a moving average filters higher frequency components without any specific connection to time, although typically some kind of ordering is implied. Viewed simplistically it can be regarded as smoothing the data.

A simple example is as shown below. Let's take 5 observations (at a time) - 5,5,5,5,5. The average would be given by

$$ \frac{5 + 5 + 5 + 5 + 5}{5} = 5 $$

Now when a new observation (say 4) comes along, the moving/rolling average would be calculated as follows.

$$ \frac{5 + 5 + 5 + 5 + 4}{5} = 4.8 $$

Thus, the moving average gives us a better idea of whether the crimes have been increasing or decreasing on an average over a period of time. The time period chosen here for the analysis is 365 days i.e. 1 year.

\newpage
\section{Rolling mean of crimes}

The scripts \ref{lst:generalcrimes} was used to create this plot.

\begin{center}
	\includegraphics[width=0.9\linewidth]{general/rollingMeanCrimes.png}\\
	\captionof{figure}{Rolling Mean of All Crimes}
	\label{fig:rollingMeanCrimes}
\end{center}

\newpage
\section{Rolling mean of each crime}

The script \ref{lst:topcrimes} was used to generate the following plots.

\begin{center}
	\input{tex/rollingMeanCrimes.tex}
\end{center}



\newpage
\section{Rolling mean of count of crimes committed at different places}

The script \ref{lst:topplaces} was used to generate the following plots.

\begin{center}
	\input{tex/rollingMeanPlace.tex}
\end{center}



\newpage
\section{Arrests General}

In this section we split up the reports as to which have been followed up with an arrest, and those which have not.

\lstinputlisting[language = Python, breaklines=true, xleftmargin=2em,frame=single,framexleftmargin=1.5em, numberstyle=\tiny, numbers = left, numbersep = 5pt, caption = {Exploration of data according to arrests}, label = {lst:arrests}]{scripts/arrests.py}

\begin{center}
	\includegraphics[width=0.9\linewidth]{arrestsGeneral/dailyArrests.png}\\
	\captionof{figure}{Daily count of arrests made}
	\label{fig:dailyCountArrests}
	
	\includegraphics[width=0.9\linewidth]{arrestsGeneral/dailyArrestsArea.png}\\
	\captionof{figure}{Stacked area chart for arrests made daily}
	\label{fig:dailyArrestsArea}
	
	\includegraphics[width=0.9\linewidth]{arrestsGeneral/rollingMean.png}\\
	\captionof{figure}{Rolling avg of arrests over the years}
	\label{fig:rollingAvgArrests}
	
	\includegraphics[width=0.9\linewidth]{arrestsGeneral/arrestsPerCrime.png}\\
	\captionof{figure}{Count of arrests for top 15 crimes}
	\label{fig:arrestsCrimesTop}
	
	\includegraphics[width=0.9\linewidth]{arrestsGeneral/arrestsPerPlace.png}\\
	\captionof{figure}{Count of arrests for top 15 places of crimes}
	\label{fig:arrestsPlacesTop}
\end{center}


\newpage
\section{Rolling avg of arrests of crimes}

The script \ref{lst:arrests} was used to generate the following plots.

\begin{center}
	\input{tex/arrestsCrimes.tex}
\end{center}

\newpage
\section{Rolling avg of arrests at crime areas}

The script \ref{lst:arrests} was used to generate the following plots.

\begin{center}
	\input{tex/arrestsPlaces.tex}
\end{center}