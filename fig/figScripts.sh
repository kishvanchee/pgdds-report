#!/bin/bash


rm -r crimesCount/
rm -r fodCrimes/
rm -r fodPlaces/
rm -r general/
rm -r placeCount/
rm -r rollingMean/
rm -r rollingMeanPlace/
rm -r arrestsCrimes/
rm -r arrestsGeneral/
rm -r arrestsPlaces/

cp -r ../../crimes-chicago/images/crimesCount/ crimesCount/
cp -r ../../crimes-chicago/images/fodCrimes/ fodCrimes/
cp -r ../../crimes-chicago/images/fodPlaces/ fodPlaces/
cp -r ../../crimes-chicago/images/general/ general/
cp -r ../../crimes-chicago/images/placeCount/ placeCount/
cp -r ../../crimes-chicago/images/rollingMean/ rollingMean/
cp -r ../../crimes-chicago/images/rollingMeanPlace/ rollingMeanPlace/
cp -r ../../crimes-chicago/images/arrestsCrimes/ arrestsCrimes/
cp -r ../../crimes-chicago/images/arrestsGeneral/ arrestsGeneral/
cp -r ../../crimes-chicago/images/arrestsPlaces/ arrestsPlaces/

echo -n "" > ../tex/crimesCount.tex; for f in crimesCount/*.png; do echo "\\includegraphics[width=0.9\\linewidth]{$f}\\\\" >> ../tex/crimesCount.tex; echo -n "\captionof{figure}{Daily count of " >> ../tex/crimesCount.tex; echo " $f" | awk -F[/.] '{print $2"}"}' >> ../tex/crimesCount.tex; echo -n "\label{fig:" >> ../tex/crimesCount.tex; echo "$f" | tr '/' ' ' | awk -F[.] '{print $1"}"}' >> ../tex/crimesCount.tex; done;

echo -n "" > ../tex/placeCount.tex; for f in placeCount/*.png; do echo "\\includegraphics[width=0.9\\linewidth]{$f}\\\\" >> ../tex/placeCount.tex; echo -n "\captionof{figure}{Daily count of crimes at " >> ../tex/placeCount.tex; echo " $f" | awk -F[/.] '{print $2"}"}' >> ../tex/placeCount.tex; echo -n "\label{fig:" >> ../tex/placeCount.tex; echo "$f" | tr '/' ' ' | awk -F[.] '{print $1"}"}' >> ../tex/placeCount.tex; done;

echo -n "" > ../tex/rollingMeanCrimes.tex; for f in rollingMean/*.png; do echo "\\includegraphics[width=0.9\\linewidth]{$f}\\\\" >> ../tex/rollingMeanCrimes.tex; echo -n "\captionof{figure}{Rolling mean of " >> ../tex/rollingMeanCrimes.tex; echo " $f" | awk -F[/.] '{print $2"}"}' >> ../tex/rollingMeanCrimes.tex; echo -n "\label{fig:" >> ../tex/rollingMeanCrimes.tex; echo "$f" | tr '/' ' ' | awk -F[.] '{print $1"}"}' >> ../tex/rollingMeanCrimes.tex; done;

echo -n "" > ../tex/rollingMeanPlace.tex; for f in rollingMeanPlace/*.png; do echo "\\includegraphics[width=0.9\\linewidth]{$f}\\\\" >> ../tex/rollingMeanPlace.tex; echo -n "\captionof{figure}{Rolling mean of crimes at " >> ../tex/rollingMeanPlace.tex; echo " $f" | awk -F[/.] '{print $2"}"}' >> ../tex/rollingMeanPlace.tex; echo -n "\label{fig:" >> ../tex/rollingMeanPlace.tex; echo "$f" | tr '/' ' ' | awk -F[.] '{print $1"}"}' >> ../tex/rollingMeanPlace.tex; done;

echo -n "" > ../tex/fodCrimes.tex; for f in fodCrimes/*.png; do echo "\\includegraphics[width=0.9\\linewidth]{$f}\\\\" >> ../tex/fodCrimes.tex; echo -n "\captionof{figure}{First order difference of " >> ../tex/fodCrimes.tex; echo " $f" | awk -F[/.] '{print $2"}"}' >> ../tex/fodCrimes.tex; echo -n "\label{fig:" >> ../tex/fodCrimes.tex; echo "$f" | tr '/' ' ' | awk -F[.] '{print $1"}"}' >> ../tex/fodCrimes.tex; done;

echo -n "" > ../tex/fodPlace.tex; for f in fodPlaces/*.png; do echo "\\includegraphics[width=0.9\\linewidth]{$f}\\\\" >> ../tex/fodPlace.tex; echo -n "\captionof{figure}{First order difference of crimes at " >> ../tex/fodPlace.tex; echo " $f" | awk -F[/.] '{print $2"}"}' >> ../tex/fodPlace.tex; echo -n "\label{fig:" >> ../tex/fodPlace.tex; echo "$f" | tr '/' ' ' | awk -F[.] '{print $1"}"}' >> ../tex/fodPlace.tex; done;

echo -n "" > ../tex/arrestsCrimes.tex; for f in arrestsCrimes/*.png; do echo "\\includegraphics[width=0.9\\linewidth]{$f}\\\\" >> ../tex/arrestsCrimes.tex; echo -n "\captionof{figure}{Rolling avg of arrests for " >> ../tex/arrestsCrimes.tex; echo " $f" | awk -F[/.] '{print $2"}"}' >> ../tex/arrestsCrimes.tex; echo -n "\label{fig:" >> ../tex/arrestsCrimes.tex; echo "$f" | tr '/' ' ' | awk -F[.] '{print $1"}"}' >> ../tex/arrestsCrimes.tex; done;

echo -n "" > ../tex/arrestsPlaces.tex; for f in arrestsPlaces/*.png; do echo "\\includegraphics[width=0.9\\linewidth]{$f}\\\\" >> ../tex/arrestsPlaces.tex; echo -n "\captionof{figure}{Rolling avg of arrests at " >> ../tex/arrestsPlaces.tex; echo " $f" | awk -F[/.] '{print $2"}"}' >> ../tex/arrestsPlaces.tex; echo -n "\label{fig:" >> ../tex/arrestsPlaces.tex; echo "$f" | tr '/' ' ' | awk -F[.] '{print $1"}"}' >> ../tex/arrestsPlaces.tex; done;